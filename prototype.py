#!/usr/bin/python
# -*- coding: utf-8 -*-

import random

vocab_speechact = [
    { "Laadan" : "Bíi", "English" : "I declare:" },
    { "Laadan" : "Báa", "English" : "I ask:" },
    { "Laadan" : "Bó", "English" : "I command:" },
    { "Laadan" : "Bóo", "English" : "I request:" },
    { "Laadan" : "Bé", "English" : "I promise:" },
    { "Laadan" : "Bée", "English" : "I warn:" },
]

vocab_evidence = [
    { "Laadan" : "wa", "English" : "perceived by me" },
    { "Laadan" : "wi", "English" : "it is self-evident" },
    { "Laadan" : "we", "English" : "I perceived it in a dream" },
    { "Laadan" : "wáa", "English" : "I trust the source" },
    { "Laadan" : "waá", "English" : "though I distrust the source" },
    { "Laadan" : "wo", "English" : "hypothetically" },
    { "Laadan" : "wóo", "English" : "but I don't know for sure" },
]

vocab_verb_intransitive = [
    { "Laadan" : "áada", "English" : "smiles" },
    { "Laadan" : "áana", "English" : "sleeps" },
    { "Laadan" : "áya", "English" : "is beautiful" },
    { "Laadan" : "bedi", "English" : "learns" },
    { "Laadan" : "híya", "English" : "is small" },
    { "Laadan" : "thal", "English" : "is good" },
    { "Laadan" : "ishidi", "English" : "chatters" },
    { "Laadan" : "ishish", "English" : "fidgets" },
    { "Laadan" : "láad", "English" : "perceives" },
    { "Laadan" : "lam", "English" : "is healthy" },
    { "Laadan" : "lalom", "English" : "sings" },
    { "Laadan" : "laya", "English" : "is red" },
    { "Laadan" : "layun", "English" : "is orange" },
    { "Laadan" : "léli", "English" : "is yellow" },
]

vocab_verb_transitive = [
    { "Laadan" : "them", "English" : "needs" },
    { "Laadan" : "néde", "English" : "wants" },
    { "Laadan" : "il", "English" : "watches" },
    { "Laadan" : "bel", "English" : "takes" },
    { "Laadan" : "naya", "English" : "takes care of" },
    { "Laadan" : "rahil", "English" : "ignores" },
    { "Laadan" : "adadama", "English" : "tickles" },
    { "Laadan" : "dithal", "English" : "praises" },
    { "Laadan" : "dóhibeyóo", "English" : "folds" },
    { "Laadan" : "duth", "English" : "uses" },
    { "Laadan" : "edethi", "English" : "shares" },
    { "Laadan" : "éholob", "English" : "threatens" },
    { "Laadan" : "el", "English" : "makes" },
    { "Laadan" : "héedá", "English" : "picks up" },
]

# No plural for now, don't want to add the prefix
vocab_noun = [    
    { "Laadan" : "áabe", "English" : "book" },
    { "Laadan" : "áalaá", "English" : "butterfly" },
    { "Laadan" : "ahana", "English" : "chocolate" },
    { "Laadan" : "ana", "English" : "food" },
    { "Laadan" : "ash", "English" : "star" },
    { "Laadan" : "babí", "English" : "bird" },
    { "Laadan" : "beth", "English" : "home" },
    { "Laadan" : "bo", "English" : "mountain" },
    { "Laadan" : "boóbil", "English" : "ivy" },
    { "Laadan" : "boshum", "English" : "cloud" },
    { "Laadan" : "bremeda", "English" : "onion" },
    { "Laadan" : "dedide", "English" : "story" },
    { "Laadan" : "domid", "English" : "elephant" },
    { "Laadan" : "dumidal", "English" : "fox" },
    { "Laadan" : "duthahá", "English" : "healer" },
    { "Laadan" : "ebalá", "English" : "baker" },
    { "Laadan" : "ededal", "English" : "pasta" },
    { "Laadan" : "elashemid", "English" : "squirrel" },
    { "Laadan" : "háanáal", "English" : "evening" },
    { "Laadan" : "háasháal", "English" : "morning" },
    { "Laadan" : "heyi", "English" : "pain" },
    { "Laadan" : "ili", "English" : "water" },
    { "Laadan" : "laheb", "English" : "spice" },
    { "Laadan" : "lan", "English" : "friend" },
    { "Laadan" : "lanemid", "English" : "dog" },
    { "Laadan" : "rul", "English" : "cat" },
]

vocab_pronoun = [
    { "Laadan" : "le", "English" : "I" },
    { "Laadan" : "ne", "English" : "you" },
    { "Laadan" : "be", "English" : "they" },
]

def GetSpeechAct():
    ran = random.randint( 0, len( vocab_speechact ) -1 )
    return vocab_speechact[ ran ]["Laadan"], vocab_speechact[ ran ]["English"]

def GetEvidence():
    ran = random.randint( 0, len( vocab_evidence ) -1 )
    return vocab_evidence[ ran ]["Laadan"], vocab_evidence[ ran ]["English"]

def GetTransitiveVerb():
    ran = random.randint( 0, len( vocab_verb_transitive ) -1 )
    return vocab_verb_transitive[ ran ]["Laadan"], vocab_verb_transitive[ ran ]["English"]

def GetIntransitiveVerb():
    ran = random.randint( 0, len( vocab_verb_intransitive ) -1 )
    return vocab_verb_intransitive[ ran ]["Laadan"], vocab_verb_intransitive[ ran ]["English"]

def GetNoun():
    ran = random.randint( 0, len( vocab_noun ) -1 )
    return vocab_noun[ ran ]["Laadan"], vocab_noun[ ran ]["English"]

def GetPronoun():
    ran = random.randint( 0, len( vocab_pronoun ) -1 )
    return vocab_pronoun[ ran ]["Laadan"], vocab_pronoun[ ran ]["English"]


def GeneratePoem():
    lineCount = random.randint( 3, 5 )
    laadanLines = []
    englishLines = []

    for i in range( lineCount ):
        # 1. NOUN-SUBJECT VERBS-I
        # 2. NOUN-SUBJECT VERBS-T NOUN-BJECT
        # 3. PRONOUN-SUBJECT VERBS-I
        # 4. PRONOUN-SUBJECT VERBS-T NOUN-OBJECT
        # 5. PRONOUN-SUBJECT VERBS-T PRONOUN-OBJECT

        laadanSpeechAct, englishSpeechAct = GetSpeechAct()
        laadanEvidence, englishEvidence = "", ""
        laadanObject, englishObject = GetNoun()

        if ( laadanSpeechAct == "Bíi" or laadanSpeechAct == "Bée" ):
            laadanEvidence, englishEvidence = GetEvidence()
            englishEvidence = ", " + englishEvidence
            laadanEvidence = " " + laadanEvidence
        
        sentenceType = random.randint( 1, 5 )

        if ( sentenceType == 1 ):
            laadanSubject, englishSubject = GetNoun()
            laadanVerb, englishVerb = GetIntransitiveVerb()

            laadanLine = laadanSpeechAct + " " + laadanVerb + " " + laadanSubject + laadanEvidence
            englishLine = englishSpeechAct + " the " + englishSubject + " " + englishVerb + englishEvidence

        elif ( sentenceType == 2 ):
            laadanSubject, englishSubject = GetNoun()
            laadanObject, englishObject = GetNoun()
            laadanVerb, englishVerb = GetTransitiveVerb()

            if ( laadanObject[-1:] == "a" or laadanObject[-1:] == "e" or laadanObject[-1:] == "i" or laadanObject[-1:] == "u" or laadanObject[-1:] == "o" ):
                laadanObject += "th"
            else:
                laadanObject += "eth"
            
            laadanLine = laadanSpeechAct + " " + laadanVerb + " " + laadanSubject + " " + laadanObject + laadanEvidence
            englishLine = englishSpeechAct + " the " + englishSubject + " " + englishVerb + " the " + englishObject + englishEvidence
            
        elif ( sentenceType == 3 ):
            laadanSubject, englishSubject = GetPronoun()
            laadanVerb, englishVerb = GetIntransitiveVerb()

            laadanLine = laadanSpeechAct + " " + laadanVerb + " " + laadanSubject + laadanEvidence
            englishLine = englishSpeechAct + " " + englishSubject + " " + englishVerb + englishEvidence

        elif ( sentenceType == 4 ):
            laadanSubject, englishSubject = GetPronoun()
            laadanObject, englishObject = GetNoun()
            laadanVerb, englishVerb = GetTransitiveVerb()

            if ( laadanObject[-1:] == "a" or laadanObject[-1:] == "e" or laadanObject[-1:] == "i" or laadanObject[-1:] == "u" or laadanObject[-1:] == "o" ):
                laadanObject += "th"
            else:
                laadanObject += "eth"
            
            laadanLine = laadanSpeechAct + " " + laadanVerb + " " + laadanSubject + " " + laadanObject + laadanEvidence
            englishLine = englishSpeechAct + " " + englishSubject + " " + englishVerb + " the " + englishObject + englishEvidence
            
        elif ( sentenceType == 5 ):
            laadanSubject, englishSubject = GetPronoun()
            laadanObject, englishObject = GetPronoun()
            laadanVerb, englishVerb = GetTransitiveVerb()

            if ( laadanObject[-1:] == "a" or laadanObject[-1:] == "e" or laadanObject[-1:] == "i" or laadanObject[-1:] == "u" or laadanObject[-1:] == "o" ):
                laadanObject += "th"
            else:
                laadanObject += "eth"
            
            laadanLine = laadanSpeechAct + " " + laadanVerb + " " + laadanSubject + " " + laadanObject + laadanEvidence
            englishLine = englishSpeechAct + " " + englishSubject + " " + englishVerb + " " + englishObject + englishEvidence

        if ( laadanSpeechAct == "Báa" ):
            laadanLine += "?"
            englishLine += "?"
        else:
            laadanLine += "."
            englishLine += "."
        
        laadanLines.append( laadanLine )
        englishLines.append( englishLine )

    return laadanLines, englishLines

again = "yes"

while ( again != "no" ):
    laadanLines, englishLines = GeneratePoem()

    for i in range( len( laadanLines ) ):
        print( laadanLines[i] )

    print( "" )
    for i in range( len( englishLines ) ):
        print( englishLines[i] )

    print( "\n\n" )
    again = input( "Again? (yes/no): " )


    
poemFile = open( "poems.txt", "w" )
for i in range( 10 ):

    poemFile.write( str( i + 1 ) + "\n" )

    for i in range( len( laadanLines ) ):
        print( laadanLines[i] )
        poemFile.write( laadanLines[i] + "\n" )

    print( "" )
    poemFile.write( "\n" )
    for i in range( len( englishLines ) ):
        print( englishLines[i] )
        poemFile.write( englishLines[i] + "\n" )

    poemFile.write( "\n-------------------------------\n" );
